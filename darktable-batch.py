#!/usr/bin/env python3
import os
import subprocess
import confight
import argparse
import re

"""
usage: darktable-cli <input file> [<xmp file>] <output file> [options] [--core <darktable options>]

options:
   --width <max width> default: 0 = full resolution
   --height <max height> default: 0 = full resolution
   --bpp <bpp>, unsupported
   --hq <0|1|false|true> default: true
   --upscale <0|1|false|true>, default: false
   --style <style name>
   --style-overwrite
   --apply-custom-presets <0|1|false|true>, default: true
   --verbose
   --help,-h
   --version
"""

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()

parser = argparse.ArgumentParser()

parser.add_argument('--width',type=int,help="--width <max width> default: 0 = full resolution")
parser.add_argument('--height',type=int,help="--height <max height> default: 0 = full resolution")
parser.add_argument('--bpp',type=int,help="--bpp <bpp>, unsupported")
parser.add_argument('--style',type=str,help="--style <style name>")
parser.add_argument('--style-overwrite', action='store_true')

hq_parser = parser.add_mutually_exclusive_group(required=False)
hq_parser.add_argument('--hq', dest='hq', action='store_true')
hq_parser.add_argument('--no-hq', dest='hq', action='store_false')

upscale_parser = parser.add_mutually_exclusive_group(required=False)
upscale_parser.add_argument('--upscale', dest='upscale', action='store_true')
upscale_parser.add_argument('--no-upscale', dest='upscale', action='store_false')

apply_custom_presets_parser = parser.add_mutually_exclusive_group(required=False)
apply_custom_presets_parser.add_argument('--apply-custom-presets', dest='apply_custom_presets', action='store_true')
apply_custom_presets_parser.add_argument('--no-apply-custom-presets', dest='apply_custom_presets', action='store_false')

skip_processed_parser = parser.add_mutually_exclusive_group(required=False)
skip_processed_parser.add_argument('--skip-processed', dest='skip_processed', action='store_true')
skip_processed_parser.add_argument('--no-skip-processed', dest='skip_processed', action='store_false')

parser.add_argument('--verbose', action='store_true')

parser.add_argument('--source_dir',type=str,help="Source dir to process")
parser.add_argument('--destination_dir',type=str,help="Destination dir for processed pictures")

parser.add_argument('--profile',default='default',type=str,help="Profile")

parser.add_argument('--exclude',type=str,help="Exclude regex")

parser.add_argument('--output-format',type=str,help="Output Format")

parser.add_argument('--darktable-cli-path',type=str,help="darktable-cli path")

parser.set_defaults(hq=None,upscale=None,apply_custom_presets=None,skip_processed=None,style_overwrite=None,verbose=None)

args = parser.parse_args()

#Populate defaults from confight
default_configs=confight.load_user_app('darktable-batch')
if args.width is None:
    args.width = default_configs[args.profile].get('width',None)
if args.height is None:
    args.height = default_configs[args.profile].get('height',None)
if args.bpp is None:
    args.bpp = default_configs[args.profile].get('bpp',None)
if args.style is None:
    args.style = default_configs[args.profile].get('style',None)
if args.style_overwrite is None:
    args.style_overwrite = default_configs[args.profile].get('style-overwrite',None)
if args.hq is None:
    args.hq = default_configs[args.profile].get('hq',None)
if args.upscale is None:
    args.upscale = default_configs[args.profile].get('upscale',None)
if args.apply_custom_presets is None:
    args.apply_custom_presets = default_configs[args.profile].get('apply-custom-presets',None)
if args.skip_processed is None:
    args.skip_processed = default_configs[args.profile].get('skip-processed',True)
if args.verbose is None:
    args.verbose = default_configs[args.profile].get('verbose',None)
if args.source_dir is None:
    args.source_dir = default_configs[args.profile].get('source-dir',None)
if args.destination_dir is None:
    args.destination_dir = default_configs[args.profile].get('destination-dir',None)
if args.exclude is None:
    args.exclude = default_configs[args.profile].get('exclude','.*\.xmp')
if args.output_format is None:
    args.output_format = default_configs[args.profile].get('output_format','jpg')
if args.darktable_cli_path is None:
    args.darktable_cli_path = default_configs[args.profile].get('darktable-cli-path','/usr/bin/darktable-cli')

#print(args)

#Build CMD Arguments
cmd_args=[]
if args.width is not None:
    cmd_args = cmd_args + ["--width",args.width]
if args.height is not None:
    cmd_args = cmd_args + ["--height",args.height]
if args.bpp is not None:
    cmd_args = cmd_args + ["--bpp",args.bpp]
if args.style is not None:
    cmd_args = cmd_args + ["--style",args.style]
if args.style_overwrite is not None:
    cmd_args = cmd_args + ["--style-overwrite"]
if args.hq is not None:
    cmd_args = cmd_args + ["--hq",str.lower(str(args.hq))]
if args.upscale is not None:
    cmd_args = cmd_args + ["--upscale",str.lower(str(args.upscale))]
if args.apply_custom_presets is not None:
    cmd_args = cmd_args + ["--apply-custom-presets",str.lower(str(args.apply_custom_presets))]
if args.verbose is not None:
    cmd_args = cmd_args + ["--verbose"]

#print(cmd_args)

source_files = os.listdir(args.source_dir)

source_files_without_excluded=[]
for file in source_files:
    if not re.match(args.exclude, str(file)):
        source_files_without_excluded.append(str(file))

iter=0
total_files=len(source_files_without_excluded)

destination_files = os.listdir(args.destination_dir)
devnull = open(os.devnull, 'w')
for file in source_files_without_excluded:
    file_name = file.split(".")[0]
    if not (args.skip_processed and f"{file_name}.{args.output_format}" in destination_files):
        subprocess.call([args.darktable_cli_path]+[f"{args.source_dir}/{file}",f"{args.destination_dir}/{file_name}.{args.output_format}"]+cmd_args,stdout=devnull, stderr=devnull)
        #print([args.darktable_cli_path]+[f"{args.source_dir}/{file}",f"{args.destination_dir}/{file_name}.{args.output_format}"]+cmd_args)
    printProgressBar(iter + 1, total_files, prefix = 'Progress:', suffix = 'Complete', length = 50)
    iter = iter + 1
